package utilitaires;

import entites.Judoka;
import static java.util.Collections.*;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;


public class UtilTriListe {
    
    public static void trierLesJudokasParNomPrenom(List<Judoka> lp){
          sort(lp, new ComparateurJudokasParNomPrenom());
    }
    
    public static  void trierLesJudokasParSexeNomPrenom(List<Judoka> lp){
          sort(lp, new ComparateurJudokasParSexeNomPrenom());
    }
    
    public static  void trierLesJudokasParPoids(List<Judoka> lp){
          sort(lp, new ComparateurJudokasParPoids());
    }
    
    public static  void trierLesJudokasParNom(List<Judoka> lp){
          sort(lp, new ComparateurJudokasParNom());
    }
    
    public static  void trierLesJudokasParVilles(List<Judoka> lp){
          sort(lp, new ComparateurJudokasParVilles());
    }
    
    public static  void trierLesJudokasParNbVictoiresDecroissant(List<Judoka> lp){
          
          sort(lp, new ComparateurJudokasParNbVictoiresDecroissant());
          reverse(lp);
    }  

    public static List<Judoka> teteDeListe(List<Judoka> liste, int nb){
      
      List<Judoka> resultat= new LinkedList<Judoka>(); 
      int i=0;
      for( Judoka elem : liste){
          resultat.add(elem);
          i++;
          if( i==nb) break;
      }
      return resultat;
    
    }

    
    
    static class  ComparateurJudokasParNbVictoiresDecroissant implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Judoka p  = (Judoka)   t ;
          Judoka p1 = (Judoka)  t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nb de victoires">
        
           if       ( p.getNbVictoires()  > p1.getNbVictoires())   resultat =   1;
           else if  ( p.getNbVictoires()  < p1.getNbVictoires() )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
    }    
}
 
 static class ComparateurJudokasParNom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0; 
        
        Judoka p  = (Judoka)  t ;
        Judoka p1 = (Judoka)  t1;
            
         //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom">
            
         resultat=  p.getNom().compareTo(p1.getNom());
            
            //</editor-fold>
        
       
        
        return resultat;  
    }    
}    
  static class ComparateurJudokasParNomPrenom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Judoka p  = (Judoka)  t ;
        Judoka p1 = (Judoka)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom/prénom">
            
            resultat=  p.getNom().compareTo(p1.getNom());
            if (resultat==0) resultat= p.getPrenom().compareTo(p1.getPrenom());
            
        //</editor-fold>
         
        return resultat;  
    }    
}  
 
  
 static class ComparateurJudokasParVilles implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Judoka p  = (Judoka)  t ;
        Judoka p1 = (Judoka)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom/prénom">
            
            resultat=  p.getVille().compareTo(p1.getVille());
           
            
        //</editor-fold>
         
        return resultat;  
    }    
}  
    
  
  
 static class ComparateurJudokasParSexeNomPrenom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Judoka p  = (Judoka)  t ;
        Judoka p1 = (Judoka)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par sexe/nom/prénom">
           
        resultat= p.getSexe().compareTo(p1.getSexe());
        if ( resultat==0){
             resultat=  p.getNom().compareTo(p1.getNom());
             if (resultat==0) resultat= p.getPrenom().compareTo(p1.getPrenom());
         }            
        
        //</editor-fold>
      
        return resultat;  
    }    
} 
  
 static class ComparateurJudokasParPoids implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Judoka p  =  (Judoka)   t ;
          Judoka p1 = (Judoka)  t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par poids">
        
           if        ( p.getPoids()   > p1.getPoids() )   resultat =   1;
           else                                           resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
    }    
}

 
}


