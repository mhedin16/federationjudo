package utilitaires;

import java.util.HashMap;
import java.util.Map;

public class UtilDojo {
      
   public  static  String[] categories    = { "super-légers","mi-légers","légers",
                                                              "mi-moyens","moyens",
                                                              "mi-lourds","lourds"
                                            };
    
   static  int[]    limitesHommes = {60, 66, 73, 81, 90, 100};
   static  int[]    limitesFemmes = {48, 52, 57, 63, 70, 78};
  
   public static String determineCategorie(String sexe, int poids){
     
     return categories[determineIndiceCategorie(sexe,poids)];       
   }  
   
   public static int    determineIndiceCategorie(String sexe, int poids){

     return sexe.equals("M")?chercheIndice( poids, limitesHommes )
                            :chercheIndice( poids, limitesFemmes );     
   }  
   
   private static int   chercheIndice(int poids, int[] tableauLimites ){
   
     int i;
     for(i=0;i<tableauLimites.length;i++) if( poids<tableauLimites[i] ) break; 
     return i;
   }  
   
       
   public static String couleurHexaCeinture(String couleur){
    
          Map<String,String> codeCouleurs=new HashMap<String, String>();
        
          codeCouleurs.put("Blanche", "#FFFFFF");
          codeCouleurs.put("Jaune",  "#FFFF00");
          codeCouleurs.put("Orange","#FF9900");
          codeCouleurs.put("Verte","#006600");
          codeCouleurs.put("Bleue", "#000099");
          codeCouleurs.put("Marron", "#663300" ); 
          codeCouleurs.put("Noire","#000000" );
          codeCouleurs.put("Rouge" , "#CC0000" );
        
          return    codeCouleurs.get(couleur);
    }
}
   