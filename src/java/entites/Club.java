package entites;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Club implements Serializable {
   
  @Id
  private String         codeClub;
    
  private String         nomClub;
  private String         adrClub;
    
  @OneToMany( mappedBy = "leClub")
  private List<Judoka>   lesJudokas;         

    public Club() {lesJudokas=new LinkedList<Judoka>();
    }
 
  
  
  //<editor-fold defaultstate="collapsed" desc="Méthodes métiers">
  
  public Float ageMoyen(){
   
      int total=0;
      Float moyenne=0F;
      int nbJdk=0;
      for (Judoka jdk : lesJudokas ){
      
         total+=jdk.getAge();
         nbJdk++;
      }
      
      moyenne= (float) total/nbJdk;
      
      return moyenne;
   
   }
   
  
  public Float poidMoyen(){
      
      Float moyenne=0F;
      int   total=0;
      int   nbJdk=0;
      
      for (Judoka jdk : lesJudokas ){
       
            total+=jdk.getPoids(); nbJdk++;        
      }
    
      if ( nbJdk>0) {moyenne= (float) total/nbJdk;}
      
      return moyenne;
  } 
  
  public Float poidMoyen(String pSexe){
      
      Float moyenne=0F;
      int   total=0;
      int   nbJdk=0;
      
      for (Judoka jdk : lesJudokas ){
      
          if ( jdk.getSexe().equals(pSexe)){
            total+=jdk.getPoids(); nbJdk++;    
         } 
      }
    
      if ( nbJdk>0) {moyenne= (float) total/nbJdk;}
      
      return moyenne;
 
  } 
  
  public int nbJudokas(){ return lesJudokas.size();}
  
  public int nbJudokas(String pSexe){
      
      int   nbJdk=0;
      
      for (Judoka jdk : lesJudokas ){
      
          if ( jdk.getSexe().equals(pSexe)){nbJdk++;} 
      }
      return nbJdk;
  
  }
  
  public List<Judoka> lesJudokasDeCategorie(String pCategorie){
  
    List<Judoka> liste= new LinkedList<Judoka>();
    
    for( Judoka j : this.lesJudokas){
    
       if (j.getCategorie().equals(pCategorie)){
       
         liste.add(j);
       }
    }
    return liste;
  }
  
  //</editor-fold>
  
  //<editor-fold defaultstate="collapsed" desc="Getters calculés">
  
  public int  getTotalVictoires(){
      
      int total=0;
      for( Judoka j : lesJudokas) total += j.getNbVictoires();
      return total;
  }
  
  //</editor-fold>
  
  //<editor-fold defaultstate="collapsed" desc="Gets Sets">
  
  public String getCodeClub() {
      return codeClub;
  }
  
  public void setCodeClub(String codeClub) {
      this.codeClub = codeClub;
  }
  
  public String getNomClub() {
      return nomClub;
  }
  
  public void setNomClub(String nomClub) {
      this.nomClub = nomClub;
  }
  
  public String getAdrClub() {
      return adrClub;
  }
  
  public void setAdrClub(String adrClub) {
      this.adrClub = adrClub;
  }
  
  public List<Judoka> getLesJudokas() {
        return lesJudokas;
    }

  public void setLesJudokas(List<Judoka> lesJudokas) {
        this.lesJudokas = lesJudokas;
  }
  
  
  //</editor-fold>
   
  //<editor-fold defaultstate="collapsed" desc="HashCode & Equals">
  
  @Override
  public int hashCode() {
      int hash = 3;
      hash = 13 * hash + (this.codeClub != null ? this.codeClub.hashCode() : 0);
      return hash;
  }
  
  @Override
  public boolean equals(Object obj) {
      if (obj == null) {
          return false;
      }
      if (getClass() != obj.getClass()) {
          return false;
      }
      final Club other = (Club) obj;
      if ((this.codeClub == null) ? (other.codeClub != null) : !this.codeClub.equals(other.codeClub)) {
          return false;
      }
      return true;
  }
  //</editor-fold> 
}


