package web.controleurs;

import donnees.Dao;
import entites.Club;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

@ManagedBean
@ViewScoped
public class Controleur06 implements Serializable {
       
      @Inject Dao dao;
    
    private Club            club;
  
    @PostConstruct
    public void init(){
    
     club= new Club();
    }
    
    public void creeClub(){
      if(dao.getClubDeCode(club.getCodeClub())==null){
          
         dao.enregister(club);
      }
    }
    
    //<editor-fold defaultstate="collapsed" desc="gets sets">
    
    public Club getClub() {
        return club;
    }
    
    public void setClub(Club club) {
        this.club = club;
    }
    
    //</editor-fold> 
   
}


