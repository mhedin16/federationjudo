package web.controleurs;

import donnees.Dao;
import entites.Club;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

@ManagedBean
@ViewScoped
public class Controleur031 implements Serializable {
     
      @Inject Dao dao;
    private List<Club> lesClubs;
  
     
    @PostConstruct
    public void init(){
        
        lesClubs=dao.getTousLesClubs();
      
    }

    

    public List<Club> getLesClubs() {
        return lesClubs;
    }

    
}
