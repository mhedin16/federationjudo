
package web.controleurs;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class Controleur00 implements Serializable{
   
    public void initialiseSession() {
       FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }
}
