package web.controleurs;

import donnees.Dao;
import entites.Judoka;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import utilitaires.UtilDojo;

@ManagedBean
@ViewScoped
public class Controleur03 implements Serializable {
     
      @Inject Dao dao;
    private List<Judoka>       lesJudokas;
  
    @PostConstruct
    public void init(){
          
        lesJudokas=dao.getTousLesJudokas();
    }
         

    
    //<editor-fold defaultstate="collapsed" desc="gets sets">
    
    public List<Judoka> getLesJudokas() {
        return lesJudokas;
    }
    
    //</editor-fold> 
}
