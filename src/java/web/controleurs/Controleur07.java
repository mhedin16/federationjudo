package web.controleurs;

import donnees.Dao;
import entites.Club;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

@ManagedBean
@ViewScoped
public class Controleur07 implements Serializable {
     
      @Inject Dao dao;
    private String       codeclub;
    private Club         club=new Club();
  
    public void rechercheClub(){
            
        if (codeclub != null) {
            club= dao.getClubDeCode(codeclub); 
        }
    }
    
    public void modifierClub(){
    
      dao.modifier(club);
    
    }
    
    //<editor-fold defaultstate="collapsed" desc="gets sets">
    
    public String getCodeclub() {
        return codeclub;
    }
    
    public void setCodeclub(String codeclub) {
        this.codeclub = codeclub;
    }
    
    public Club getClub() {
        return club;
    }
    
    public void setClub(Club club) {
        this.club = club;
    }
    
    //</editor-fold> 
   
}


