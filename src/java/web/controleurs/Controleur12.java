
package web.controleurs;

import donnees.Dao;
import entites.Club;
import entites.Judoka;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.chart.PieChartModel;

@ManagedBean
@ViewScoped
public class Controleur12  implements Serializable{
    
    @Inject Dao dao;
    private List<Club> lesClubs;
    private PieChartModel        pieModel;
    @PostConstruct
    public void init(){
        Map<String, Long>    tableAssoc= new TreeMap<String, Long>();
         
       for (Judoka j : dao.getTousLesJudokas()){
        
          String cat= j.getCategorie();
          tableAssoc.put(cat, tableAssoc.containsKey(cat)?tableAssoc.get(cat)+1:1);
       } 
         pieModel= new  PieChartModel();
        
       for (String categ : tableAssoc.keySet()){
        
           pieModel.set(categ,tableAssoc.get(categ));
    
       }
       
     
       pieModel.setTitle("Répartition par catégories de poids");
       pieModel.setLegendPosition("se");
       pieModel.setFill(true);
       pieModel.setShowDataLabels(true);
       pieModel.setSliceMargin(8);
       pieModel.setDiameter(300);
    }
    public PieChartModel getPieModel() {
        return pieModel;
    }
}
