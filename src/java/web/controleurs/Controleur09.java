package web.controleurs;

import donnees.Dao;
import entites.Club;
import entites.Judoka;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import utilitaires.UtilDojo;

@ManagedBean
@ViewScoped
public class Controleur09 implements Serializable {
     
      @Inject Dao dao;
    private String       codeclub;
    
    private Club         club;
    
    private Judoka       judoka= new Judoka();
  
    public void rechercheClub(){
            
        if (codeclub != null) {
            club= dao.getClubDeCode(codeclub);
          
        }
    }
    
    public void ajouterJudoka(){
        
       if ( dao.getJudokaNumero(judoka.getId())==null){ 
       
          judoka.setLeClub(club);
          club.getLesJudokas().add(judoka);
    
          dao.enregister(judoka);
          dao.modifier(club);
          
       }
    }
    
    
    public String codeCouleurCeinture(Judoka j){
    
         return UtilDojo.couleurHexaCeinture(j.getCeinture());      
    }
    
 
    //<editor-fold defaultstate="collapsed" desc="gets sets">
    
    public String getCodeclub() {
        return codeclub;
    }
    
    public void setCodeclub(String codeclub) {
        this.codeclub = codeclub;
    }
    
    public Club getClub() {
        return club;
    }
    
    public void setClub(Club club) {
        this.club = club;
    }
    
     public Judoka getJudoka() {
        return judoka;
    }

    public void setJudoka(Judoka judoka) {
        this.judoka = judoka;
    }
    
    
    //</editor-fold> 

   
   
}


