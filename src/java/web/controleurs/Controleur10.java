package web.controleurs;

import donnees.Dao;
import entites.Judoka;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

@ManagedBean
@ViewScoped
public class Controleur10 implements Serializable {
      
      @Inject Dao dao;
    
    private Judoka         judoka=new Judoka();
  
    public void rechercheJudoka(){
        judoka.setPoids(0);judoka.setSexe("");
        if (judoka.getId() != null) {
            judoka= dao.getJudokaNumero(judoka.getId()); 
        }
    }
    
    public void modifierJudoka(){
    
     dao.modifier(judoka);
    
    }
    
    //<editor-fold defaultstate="collapsed" desc="gets sets">
    
     public Judoka getJudoka() {
        return judoka;
    }

    public void setJudoka(Judoka judoka) {
        this.judoka = judoka;
    }
   
    
    //</editor-fold> 

   
}


