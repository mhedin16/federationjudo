package web.controleurs;

import donnees.Dao;
import entites.Judoka;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.chart.PieChartModel;

@ManagedBean
@ViewScoped
public class Controleur05 implements Serializable {
      
     @Inject Dao dao; 
    private PieChartModel        pieModel;
    
    @PostConstruct
    public void init(){
         
      List<String>   couleurs = new ArrayList(Arrays.asList
                         (              
                           "Blanche","Jaune","Orange","Verte","Bleue","Marron","Noire","Rouge"
                         )
                         );
      List<Integer>   nb= new ArrayList(Arrays.asList(0,0,0,0,0,0,0,0));
                          
      for(Judoka j : dao.getTousLesJudokas()){
      
         int index= couleurs.indexOf(j.getCeinture());
         nb.set(index, nb.get(index)+1);
      }  
       
      
       pieModel= new  PieChartModel();
        
       for (int i=0;i<nb.size();i++){
       
         pieModel.set(couleurs.get(i), nb.get(i));
       } 
      
    
      // Blanche: FFFFFF
      // Jaune  : FFFF00
      // Orange : FF9900
      // Verte  : 006600
      // Bleue  : 000099
      // Marron : 663300 
      // Noire  : 000000 
      // Rouge  : CC0000 
        
       pieModel.setSeriesColors("FFFFFF,FFFF00,FF9900,006600,000099,663300,000000,CC0000");
       pieModel.setTitle("Répartition des ceintures");
       pieModel.setLegendPosition("se");
       pieModel.setFill(true);
       pieModel.setShowDataLabels(true);
       pieModel.setSliceMargin(8);
       pieModel.setDiameter(300);
    }

    public PieChartModel getPieModel() {
        return pieModel;
    }
    
   }

   
    



