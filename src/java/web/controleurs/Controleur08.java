package web.controleurs;

import donnees.Dao;
import entites.Club;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

@ManagedBean
@ViewScoped
public class Controleur08 implements Serializable {
     
      @Inject Dao dao;
    
    private Club         club=new Club();
  
    public void rechercheClub(){
            
        if (club.getCodeClub() != null) {
            club= dao.getClubDeCode(club.getCodeClub()); 
        }
    }
    
    public void supprimerClub(){
    
     if(club.getLesJudokas().isEmpty()){
         dao.supprimer(club);
         club=new Club();
     }
    
    }
    
    //<editor-fold defaultstate="collapsed" desc="gets sets">
    
    public Club getClub() {
        return club;
    }
    
    public void setClub(Club club) {
        this.club = club;
    }
    
    //</editor-fold> 
   
}


