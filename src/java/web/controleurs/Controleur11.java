package web.controleurs;

import donnees.Dao;
import entites.Judoka;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import utilitaires.UtilDojo;

@ManagedBean
@ViewScoped
public class Controleur11 implements Serializable {
      
      @Inject Dao dao;
    
    private Judoka         judoka=new Judoka();
  
    public void rechercheJudoka(){
        
        System.out.println(judoka.getId());
        
        if (judoka.getId() != null) {
            judoka= dao.getJudokaNumero(judoka.getId()); 
        }
    }
    
    public void supprimerJudoka(){
    
      judoka.getLeClub().getLesJudokas().remove(judoka);  
      dao.supprimer(judoka);
      judoka= new Judoka();
    }
    
     public String codeCouleurCeinture(){
    
         return judoka!=null?UtilDojo.couleurHexaCeinture(judoka.getCeinture()):"";      
    }
    
    //<editor-fold defaultstate="collapsed" desc="gets sets">
    
     public Judoka getJudoka() {
        return judoka;
    }

    public void setJudoka(Judoka judoka) {
        this.judoka = judoka;
    }
   
    
    //</editor-fold> 

   
}


